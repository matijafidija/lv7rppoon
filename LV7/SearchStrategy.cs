﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7
{
    public abstract class SearchStrategy
    {
        public abstract int Search(double num, double[] arr);
    }
}
