﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1, 8, 6, 4, 34, -1, -100, 100 };

            NumberSequence NumSeq = new NumberSequence(array);
            BubbleSort BubSrt = new BubbleSort();
            NumSeq.SetSortStrategy(BubSrt);

        }
    }
}
